// Base URL for all API calls
const BASE_URL = 'https://www.thesportsdb.com/api/v1/json/1';

// URL Endpoints
const urlSportsEndpoint = '/all_sports.php?';
const urlLeaguesEndPoint = '/all_leagues.php?';
let urlTeamsEndPoint = '/search_all_teams.php?';

// Combined URLs
const urlSearchSports = `${BASE_URL}${urlSportsEndpoint}`;
const urlSearchLeagues = `${BASE_URL}${urlLeaguesEndPoint}`;
let urlSearchTeams = `${BASE_URL}${urlTeamsEndPoint}`;

// Empty arrays used to update dropdown lists
let sportList = [];
let leagueList = [];
let teamList = [];

// API call to get a list of sports
fetch(urlSearchSports)
	.then(function(response) {
	return response.json();
})
	.then(function(responseJson) {
	for (let i = 0; i < responseJson.sports.length; i++) {
		sportList.push(responseJson.sports[i].strSport);
	}

	// Add to dropdown menu
	const sportsEl = document.getElementById('sports');
	updateDropdown(sportsEl, sportList,'Sport');
})

// UPDATE DROPDOWN LIST OF LEAGUES BASED ON SELECTED SPORT
function showLeagues() {
	// Collect selected data
	const sportsEl = document.getElementById('sports');
	const leagueEl = document.getElementById('leagues');
	let leagueOptionEl = leagueEl.getElementsByTagName('option');
	let chosenSport = sportsEl.options[sportsEl.selectedIndex].value;

	// Delete league list of previous sport
	while (leagueEl.firstChild) {
		leagueEl.firstChild.remove();
	}

	// API call to get a list of leagues based on sport selection
	fetch(urlSearchLeagues)
		.then(function(response) {
		return response.json();
	})
		.then(function(responseJson) {
		leagueList = [];
		for (let i = 0; i < responseJson.leagues.length; i++) {
			if (responseJson.leagues[i].strSport === chosenSport) {
				leagueList.push(responseJson.leagues[i].strLeague);
			}
		}
		// Update dropdown
		updateDropdown(leagueEl,leagueList,'League');
	})
}

// UPDATES DROPDOWN LIST OF TEAMS BASED ON LEAGUE SELECTION
function showTeams() {
	// Update url for API call
	const leagueEl = document.getElementById('leagues');
	const teamEl = document.getElementById('teams');
	let teamOptionEl = teamEl.getElementsByTagName('option');
	let chosenLeague = leagueEl.options[leagueEl.selectedIndex].value;
	const chosenLeagueURL = chosenLeague.replace(/\s+/g, '_');
	urlTeamsEndPoint = `/search_all_teams.php?l=${chosenLeagueURL}`;
	urlSearchTeams = `${BASE_URL}${urlTeamsEndPoint}`;

	// Delete team list of previous league
	while (teamEl.firstChild) {
		teamEl.firstChild.remove();
	}

	// API call to get list of teams based on league selection
	fetch(urlSearchTeams)
		.then(function(response) {
		return response.json();
	})
		.then(function(responseJson) {
		teamList = [];
		for (let i = 0; i < responseJson.teams.length; i++) {
			if (responseJson.teams[i].strLeague === chosenLeague) {
				teamList.push(responseJson.teams[i].strTeam);
			}
		}
		// Update dropdown
		updateDropdown(teamEl,teamList,'Team');
	})
}

// SUBMIT EVENT LISTENER
const getPlayers = addEventListener('submit', function(e) {
	// Form validation to make sure all fields are valid

	// If sport selection has not been made do not allow the form to be submitted and show an error message
	if (e.target.getElementsByTagName('select')[0].value === '') {
		e.target.getElementsByClassName('input-error')[0].innerHTML= 'Please select a sport';
		e.preventDefault();
	} 

	// If sport selection has been made remove error message
	if (e.target.getElementsByTagName('select')[0].value != '') {
		e.target.getElementsByClassName('input-error')[0].innerHTML = '';
		e.preventDefault()
	}

	// If sport selection has been made but league selection has not been made, do not allow the form to be submitted and show an error message
	if (e.target.getElementsByTagName('select')[0].value != '' && e.target.getElementsByTagName('select')[1].value === '') {
		e.target.getElementsByClassName('input-error')[1].innerHTML = 'Please select a league';
		e.preventDefault();	
	}

	// If league selection has been made remove the error message
	if (e.target.getElementsByTagName('select')[1].value != '') {
		e.target.getElementsByClassName('input-error')[1].innerHTML = '';
		e.preventDefault();
	}

	// If league selection has been made but no team selection has been made, do not allow the form to be submitted and show an error message
	if (e.target.getElementsByTagName('select')[2].value === '' && e.target.getElementsByTagName('select')[1].value != '') {
		e.target.getElementsByClassName('input-error')[2].innerHTML = 'Please select a team';
		e.preventDefault;
	}

	// If team selection has been made remove the error message
	if (e.target.getElementsByTagName('select')[2].value != '') {
		e.target.getElementsByClassName('input-error')[2].innerHTML = '';
		e.preventDefault();
	}

	// If all three selections have been made, allow the form to be submitted
	if (e.target.getElementsByTagName('select')[0].value != '' && e.target.getElementsByTagName('select')[1].value != '' && e.target.getElementsByTagName('select')[2].value != '') {

		// Remove previous logo to replace with new one
		let imgEls = document.getElementsByTagName('img');
		for (let i = 0; i < imgEls.length; i++) {
			imgEls[i].remove();
		}

		// Display new logo based on team selection
		const parentContainer = document.getElementById('parent-container');
		const newImgEl = document.createElement('img');
		parentContainer.insertBefore(newImgEl,parentContainer.childNodes[2]);

		// Update the url for API call
		leagueEl = document.getElementById('leagues');
		teamEl = document.getElementById('teams');
		let chosenLeague = leagueEl.options[leagueEl.selectedIndex].value;
		let chosenTeam = teamEl.options[teamEl.selectedIndex	].value;
		const chosenLeagueURL = chosenLeague.replace(/\s+/g, '_');
		urlTeamsEndPoint = `/search_all_teams.php?l=${chosenLeagueURL}`;
		urlSearchTeams = `${BASE_URL}${urlTeamsEndPoint}`;

		// Fetch teams from league
		fetch(urlSearchTeams)
			.then(function(response) {
			return response.json();
		})

		// Find the corresponding team and apply src attribute to the image element
			.then(function(responseJson) {
			let strTeamBadge;
			const imgEl = document.getElementsByTagName('img')[0];
			for (let i = 0; i < responseJson.teams.length; i++) {
				if (responseJson.teams[i].strTeam === chosenTeam) {
					strTeamBadge = responseJson.teams[i].strTeamBadge;
					imgEl.setAttribute('src',responseJson.teams[i].strTeamBadge);
				}

				// Show "Your Favorite" if the selection matches the url saved in local storage. If not, show "Display Favorite"
				if (strTeamBadge === localStorage.getItem('logo')) {
					document.getElementById('load-favorite').style.backgroundColor = '#E89AC9';
					document.getElementById('load-favorite').setAttribute('value','Your Favorite');
				} else {
					document.getElementById('load-favorite').style.backgroundColor = '';
					document.getElementById('load-favorite').setAttribute('value', 'Show Favorite');
				}
			}
			// Animate logo display in browser
			let imgWidth = 0;
			animateTeamLogo();
		})
	}
})

// SAVE INPUT TO LOCAL STORAGE
const saveFavorite = function(e) {
	const favoriteSport = document.getElementsByTagName('select')[0].value;
	const favoriteLeague = document.getElementsByTagName('select')[1].value;
	const favoriteTeam = document.getElementsByTagName('select')[2].value;
	const favoriteLogoSrc = document.getElementsByTagName('img')[0].src;
	if (favoriteLogoSrc != '') {
		localStorage.setItem('sport',favoriteSport);
		localStorage.setItem('league',favoriteLeague);
		localStorage.setItem('team',favoriteTeam);
		localStorage.setItem('logo',favoriteLogoSrc);
	} else {
		window.alert('Your selection could not be saved. The logo must be displayed to Set as Favorite.');
		e.preventDefault();
	}
}
const saveFavoriteButton = document.getElementById('save-favorite');
saveFavoriteButton.addEventListener('click',saveFavorite)

// EVENT LISTENER TO DISPLAY FAVORITE LOGO FROM LOCAL STORAGE
const loadFavoriteButton = document.getElementById('load-favorite');
loadFavoriteButton.addEventListener('click', function(e) {
	if (localStorage.getItem('logo') != '') {
		const favoriteLogo = localStorage.getItem('logo');
		const newImgSrc = document.getElementsByTagName('img')[0].src = favoriteLogo;
		loadFavoriteButton.setAttribute('value','Your Favorite');
		loadFavoriteButton.style.backgroundColor = '#E89AC9';
		animateTeamLogo();
	} else {
		window.alert('You do not currently have a favorite logo.');
		e.preventDefault();
	}
})

// FUNCTION TO CHECK TO SEE OBJECT IS REAL
const isRealValue = function (obj) {
	return obj && obj !== 'null' && obj !== 'undefined';
}

// FUNCTION TO UPDATE DROPDOWN MENU
const updateDropdown = function (listElement, listArray, fieldType) {
	for (let i = 0; i <= listArray.length; i++) {
		const newOptionEl = document.createElement('option');
		listElement.appendChild(newOptionEl);
		if (i === 0) {
			listElement.lastElementChild.setAttribute('disabled','');
			listElement.lastElementChild.setAttribute('selected','');
			listElement.lastElementChild.setAttribute('value','');
			listElement.lastElementChild.innerHTML = `Select ${fieldType}`;
		} else {
			listElement.lastElementChild.setAttribute('id', `${fieldType}-${i-1}`);
			listElement.lastElementChild.setAttribute('value', listArray[i-1]);
			listElement.lastElementChild.innerHTML = listArray[i-1];
		}
	}
}

// FUNCTION TO ANIMATE TEAM LOGO
const animateTeamLogo = function(url, source) {
	const imgEl = document.getElementsByTagName('img')[0];
	let imgWidth = 0;
	const animate = function() {
		imgEl.style.width = `${imgWidth}%`;
		imgWidth += 1;
		if (imgWidth <= 50) {
			requestAnimationFrame(animate);
		}
	}
	requestAnimationFrame(animate);
}